import os

from setuptools import setup

if os.environ.get("CI_COMMIT_TAG"):
    version = os.environ["CI_COMMIT_TAG"]
elif os.environ.get("CI_JOB_ID"):
    version = os.environ["CI_JOB_ID"]
else:
    version = "test"


with open("README.md", "r") as f:
    long_description = f.read()

setup(
    name="pyodstibmivb",
    version=version,
    author="Emil Vanherp",
    author_email="emil@vanherp.me",
    description="A Python wrapper for the Stib-Mivb opendata API",
    license="MIT License",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/EmilV2/pyodstibmivb",
    install_requires=["aiohttp==3.5.4"],
    python_requires=">=3",
    packages=["pyodstibmivb"],
    package_dir={"": "src"},
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
